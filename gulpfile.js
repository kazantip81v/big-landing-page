var gulp         = require('gulp'),
		sass         = require('gulp-sass'),
		autoprefixer = require('gulp-autoprefixer'),
		cleanCSS    = require('gulp-clean-css'),
		rename       = require('gulp-rename'),
		browserSync  = require('browser-sync').create(),
		concat       = require('gulp-concat'),
		uglify       = require('gulp-uglify');

gulp.task('browser-sync', ['styles', 'scripts'], function() {
		browserSync.init({
				server: {
						baseDir: "./app"
				},
				notify: false
		});
});

gulp.task('styles', function () {
	return gulp.src('sass/*.sass')
	.pipe(sass({
		includePaths: require('node-bourbon').includePaths
	}).on('error', sass.logError))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer({browsers: [
    "> 1%",
    "last 2 versions",
    "last 2 Chrome versions",
    "last 2 Firefox versions",
    "last 2 IOS versions",
    "last 2 Safari versions",
    "Explorer >= 9",
    "Edge >= 12",
    "last 2 Android versions",
    "last 2 ChromeAndroid versions"
  ], cascade: false}))
	//.pipe(cleanCSS())
	.pipe(gulp.dest('app/css'))
	.pipe(browserSync.stream());
});

gulp.task('scripts', function() {
	return gulp.src([
		'./app/libs/jquery/jquery-1.11.2.min.js',
		'./app/libs/modernizr/modernizr.js',
		'./app/libs/twentytwenty-master/js/jquery.event.move.js',
		'./app/libs/twentytwenty-master/js/jquery.twentytwenty.js',
		'./app/libs/owl-carousel/owl.carousel.min.js',
		//'./app/libs/Carousel.js',
		'./app/libs/magnificPopup/jquery.magnific-popup.min.js'
		])
		.pipe(rename({suffix: '.min', prefix : ''}))
		.pipe(concat('libs.js'))
	  	//.pipe(uglify()) //Minify libs.js
		.pipe(gulp.dest('./app/js/'));
});

gulp.task('watch', function () {
	gulp.watch('sass/*.sass', ['styles']);
	gulp.watch('app/libs/**/*.js', ['scripts']);
	gulp.watch('app/js/*.js').on("change", browserSync.reload);
	gulp.watch('app/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['browser-sync', 'watch']);
