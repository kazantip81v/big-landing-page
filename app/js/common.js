$(window).load(function(){
	var mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);

	if( mobile ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
	$('body').removeClass('loaded');
});


$(function() {

	$("a").on("dragstart", function(event) {
				event.preventDefault();
		}, false);

	// header nav
	$(".nav-toogle").click(function () {
		$(this).toggleClass("nav-toogle_opened");
		$(".top-nav").slideToggle("slow");
		$("section").toggleClass("hidden");
	});

	$(".top-nav__link").click(function () {
		$("section").toggleClass("hidden");
		$(".top-nav").slideToggle();
		$(".nav-toogle").removeClass("nav-toogle_opened");
	});

	$('nav a[href^="#"]').click(function (event) {

		let target = $( $(this).attr('href') );

		if( target.length ) {
				event.preventDefault();
				$('html, body').animate({
						scrollTop: target.offset().top+5
				}, 1000);
		}
	});

	// header twentytwenty 
	$("#twenty").twentytwenty();

	//popup
	$('a[href="#callback-popup"]').magnificPopup({
		type: 'inline',
		preloader: false,
		mainClass: 'mfp-forms',
		focus: '#nameH'
	});

	$('a[href="#modal-popup"]').magnificPopup({
		type: 'inline',
		preloader: false,
		mainClass: 'mfp-modal'
		//modal: true
	});


	//section_1
	$('.answer__link').click(function(){
		$('.answer__link').removeClass('answer__link_active');
		$(this).addClass('answer__link_active');
	});

	var discount = 0;
	var question = 1;
	var click_button = 0;

	$('.inquirer-discount .button').click(function(){
		click_button++;
		discount += 100;
		$('.js-discount').text(discount);

		if(question <= 4) {
			$('[data-answer='+ question + ']')
				.addClass('hidden')
				.next()
				.removeClass('hidden');
			question++;
			$('.steps__item.steps__item_active')
				.removeClass('steps__item_active')
				.next()
				.addClass('steps__item_active');
		}
		if(question === 5 && click_button === 5){ 
			$('a[href="#callback-popup"]').trigger('click');
			$('.js-discount').text(discount = 0);
		}
	});

	//section_6
	$('input[type=range]').change(function(){
		var list_num = $(this).val();
		$('.content__list').addClass('hidden');
		$('.content__list.list_'+ list_num).removeClass('hidden');
	});

	// section_7	
	$(".minus-ten__link").click(function () {
		$(".minus-ten-years").addClass("hidden");
		$(".minus-ten-ss").removeClass("hidden");
	});

	$(".minus-ten-ss__link").click(function () {
		$(".minus-ten-ss").addClass("hidden");
		$(".minus-ten-years").removeClass("hidden");
	});

	// owl-carousel section _4, _8, _10, _11, _13
	$(".ik-slider, .res-slider, .tu-slider, .ac-slider, .dp-slider").owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		navText: false,
		smartSpeed: 500,
	});

	//carousel.js section_14
	$(".container").carousel({
		num: 5,
		maxWidth: 278,
		maxHeight: 295,
		autoPlay: false,
		animationTime: 300,
		scale: 0.85,
		distance: 20
	});

	let slides = $('.abs-slider__item');

	slides.each(function(index, elem){
		if (elem.style.getPropertyValue('z-index') === '10001') {
			elem.style.setProperty('opacity', '1')
		} else {
			elem.style.setProperty('opacity', '0.5')
		}
	});
	
	$('.abs-slider__next, .abs-slider__prev').click(function () {
		let slides = $('.abs-slider__item');

		slides.each(function(index, elem){
			if (elem.style.getPropertyValue('z-index') === '10001') {
				elem.style.setProperty('opacity', '1')
			} else {
				elem.style.setProperty('opacity', '0.5')
			}
		});
	});


	/*
	$(".abs-slider").on('init', function(slick){
		let active = $(".slick-current");
		let prev = active.prev();
		let next = active.next();

		prev.addClass('item_prev').prev().addClass('item_p-prev');
		next.addClass('item_next').next().addClass('item_n-next');
	});

	let abs = $(".abs-slider");
	$(abs).slick({
		prevArrow: '<div class="abs-slider__prev"></div>',
		nextArrow: '<div class="abs-slider__next"></div>',
		infinite: true,
		speed: 400,
		centerMode: true,
		centerPadding: '90px',
		slidesToShow: 1,
		focusOnSelect: true,
		touchMove: false
	});
	
	$(".abs-slider__next").click(function () {
		let active = $(".slick-current");
		let prev = active.prev();
		let next = active.next();
		$('.slick-slide').each(function(index){
			$(this).removeClass('item_prev item_next item_p-prev item_n-next')
		});
		setTimeout(function(){
			prev.addClass('item_prev').prev().addClass('item_p-prev');
			next.addClass('item_next').next().addClass('item_n-next');
		}, 400);	
	});

	$(".abs-slider__prev").click(function () {
		let active = $(".slick-current");
		let prev = active.prev();
		let next = active.next();
		$('.slick-slide').each(function(index){
			$(this).removeClass('item_prev item_next item_p-prev item_n-next')
		});

		setTimeout(function(){
			prev.addClass('item_prev').prev().addClass('item_p-prev');
			next.addClass('item_next').next().addClass('item_n-next');
		}, 400);
	});
	*/

	// owl-slider section_12  and youtube-box
	let ltv = $(".ltv-slider");
	$(ltv).owlCarousel({
		items: 1,
		video: true,
		loop: true,
		nav: true,
		navText: false,
		smartSpeed: 500,
		onTranslated: video_delete
	});

	function video_delete(event) {
			$(ltv).each(function() {
				 $(this).find('iframe').remove();
			});
		};

	$('.youtube-box').each(function() {
		let play = $(this).find('.youtube-box__play');
				play.css('background-image', 'url(http://img.youtube.com/vi/' + play.data('youtubeid') + '/0.jpg)');
	});
	$('.youtube-box__play').click(function() {
		let video = '<iframe src="http://www.youtube.com/embed/' + $(this).data('youtubeid') + '?autoplay=1" allowfullscreen></iframe>'
		$(this).closest('.youtube-box').append(video);
			return false;
	});
	
	// section_15
	$(".a-serv__item").click(function () {

		$(this)
			.toggleClass("a-serv__item_active")
			.next(".a-serv__item_detail")
			.slideToggle();
	});

	//E-mail Ajax Send
	//Documentation & Example: https://github.com/agragregra/uniMail
	$('form').submit(function() { //Change
		var self_this = $(this);
		$.ajax({
			//type: 'POST',
			//url: 'mail.php', //Change
			//data: self_this.serialize()
		}).done(function() {
			$('a[href="#modal-popup"]').trigger('click');
			
			setTimeout(function() {
				// Done Functions
				self_this.trigger('reset');
				//$('.mfp-close').trigger('click');
			}, 500);
		});
		return false;
	});


});